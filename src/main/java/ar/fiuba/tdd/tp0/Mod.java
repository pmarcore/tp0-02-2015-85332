package ar.fiuba.tdd.tp0;

import java.util.Stack;

public class Mod implements Operator{

	@Override
	public float operate(Stack<Float> operands) {
		float op2 = operands.pop().floatValue();
		return operands.pop().floatValue() % op2;
	}

}
