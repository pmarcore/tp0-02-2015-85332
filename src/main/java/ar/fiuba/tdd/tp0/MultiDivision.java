package ar.fiuba.tdd.tp0;

import java.util.Stack;

public class MultiDivision implements Operator{

	@Override
	public float operate(Stack<Float> operands) {
		float div = operands.pop().floatValue();
		
		while (!operands.isEmpty()){
			div = div / operands.pop().floatValue();
		}	
		
		return div;
	}
}
