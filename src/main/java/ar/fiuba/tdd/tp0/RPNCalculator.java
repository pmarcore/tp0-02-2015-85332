package ar.fiuba.tdd.tp0;
import java.util.*;

public class RPNCalculator {
	
	private OperationsFactory factory = new OperationsFactory();  
	
	public float eval(String expression) {
		
		if (expression == null) throw new IllegalArgumentException();
		
		Stack<Float> operands = new Stack<Float>();
		String[] symbols = expression.split("\\s+");
		Operator operator;
		
		for (String symbol: symbols){
			
			if (symbol.matches("^[0-9]+|^-[0-9]+")){
				float f = Float.parseFloat(symbol);
				operands.push(new Float(f));
			}
			else {
				operator = this.factory.newOperator(symbol);
				operands.push(operator.operate(operands));
			}			
		}
		
		return operands.pop().floatValue();
    }
}
