package ar.fiuba.tdd.tp0;

import java.util.Stack;

public class Sum implements Operator {

	@Override
	public float operate(Stack<Float> operands) {
		
		if (operands.size() < 2) throw new IllegalArgumentException();
		return operands.pop().floatValue() + operands.pop().floatValue();
	}

}
