package ar.fiuba.tdd.tp0;

import java.util.Stack;

public class Multiplication implements Operator{

	@Override
	public float operate(Stack<Float> operands) {
		
		return operands.pop().floatValue() * operands.pop().floatValue();
	}

}
