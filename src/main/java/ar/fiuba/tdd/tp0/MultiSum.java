package ar.fiuba.tdd.tp0;

import java.util.Stack;

public class MultiSum implements Operator{

	@Override
	public float operate(Stack<Float> operands) {
		float sum = 0;
		
		if (operands.isEmpty()) throw new IllegalArgumentException();
		
		while (!operands.isEmpty()){
			sum += operands.pop().floatValue();
		}
		
		return sum;
	}
	
}
