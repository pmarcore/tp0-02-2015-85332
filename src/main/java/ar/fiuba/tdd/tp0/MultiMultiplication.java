package ar.fiuba.tdd.tp0;

import java.util.Stack;

public class MultiMultiplication implements Operator{

	@Override
	public float operate(Stack<Float> operands) {
		float mult = operands.pop().floatValue();
		
		while (!operands.isEmpty()){
			mult = mult * operands.pop().floatValue();
		}
		return mult;
	}
}
