package ar.fiuba.tdd.tp0;

import java.util.Stack;

public class MultiSubtract implements Operator{

	@Override
	public float operate(Stack<Float> operands) {
		float sustraendo = 0;
		
		while (!(operands.size() == 1)){
			sustraendo -= operands.pop();
			System.out.println(sustraendo);
		}
		
		return operands.pop() + sustraendo;
	}

}
