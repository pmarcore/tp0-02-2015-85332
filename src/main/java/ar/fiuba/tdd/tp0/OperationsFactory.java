package ar.fiuba.tdd.tp0;

public class OperationsFactory {
	
	public Operator newOperator(String symbol){
		
		Operator op;
		
		switch (symbol){
		case "+": 
			op = new Sum();
			break;
		case "-": 
			op = new Subtract();
			break;
		case "*":
			op = new Multiplication();
			break;
		case "/":
			op = new Division();
			break;
		case "MOD":
			op = new Mod();
			break;
		case "++":
			op = new MultiSum();
			break;
		case "--":
			op = new MultiSubtract();
			break;
		case "**":
			op = new MultiMultiplication();
			break;
		case "//":
			op = new MultiDivision();
			break;
		default:
			throw new IllegalArgumentException();
		}
		
		return op;
	}

}
