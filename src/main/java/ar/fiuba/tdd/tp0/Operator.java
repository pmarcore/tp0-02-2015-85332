package ar.fiuba.tdd.tp0;

import java.util.Stack;

public interface Operator {
	
	public float operate(Stack<Float> operands);
}
